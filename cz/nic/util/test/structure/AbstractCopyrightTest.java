/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.util.test.structure;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public abstract class AbstractCopyrightTest {

    private static final String         COPYRIGHT_PATTERN   = ".*Copyright \\(C\\) \\d{4} CZ\\.NIC, z\\.s\\.p\\.o\\. \\(http://www\\.nic\\.cz/\\).*" +
                                                              "This program is free software: you can redistribute it and/or modify.*" +
                                                              "it under the terms of the GNU General Public License as published by.*" +
                                                              "the Free Software Foundation, either version 3 of the License, or.*" +
                                                              "\\(at your option\\) any later version\\..*" +
                                                              "This program is distributed in the hope that it will be useful,.*" +
                                                              "but WITHOUT ANY WARRANTY; without even the implied warranty of.*" +
                                                              "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE\\.  See the.*" +
                                                              "GNU General Public License for more details\\..*" +
                                                              "You should have received a copy of the GNU General Public License.*" +
                                                              "along with this program\\.  If not, see <http://www\\.gnu\\.org/licenses/>\\..*";

    private     static final Pattern        PATTERN                 = Pattern.compile(COPYRIGHT_PATTERN, Pattern.DOTALL);

    protected   static final String         JAVA_EXTENSION          = ".java";
    protected   static final String         PROPERTIES_EXTENSION    = ".properties";
    protected   static final String         XML_EXTENSION           = ".xml";
    protected   static final String         GRADLE_EXTENSION        = ".gradle";
    protected   static final String         YML_EXTENSION           = ".yml";

    private final String[]      paths;
    private final String[]      extensions;
    private final List<String>  excludedFiles;

    private List<String> filesToControl = new ArrayList<>();

    public AbstractCopyrightTest(String[] paths, String[] extensions, List<String> excludedFiles) {
        if (paths == null || paths.length == 0) {
            throw new IllegalArgumentException("Illegal paths argument!");
        }
        this.paths = paths;

        if (extensions == null || extensions.length == 0) {
            throw new IllegalArgumentException("Illegal extensions argument!");
        }
        this.extensions = extensions;

        if (paths.length != extensions.length) {
            throw new IllegalArgumentException("Different paths and extensions argument!");
        }
        this.excludedFiles = excludedFiles;
    }

    @Before
    public void tearUp() {
        for (int i = 0; i < paths.length; i++) {
            File file = new File(paths[i]);
            String extension = extensions[i];
            System.out.println("Checking files in: \"" + file.getAbsolutePath() + "\" with extension \"" + extension + "\"");
            loadFiles(file.listFiles(), extension);
        }
    }

    @Test
    public void copyrightTest() {
        int filesContainsCopyright = 0;
        List<String> validFiles = new ArrayList<>();
        List<String> invalidFiles = new ArrayList<>();
        for(String filePath : filesToControl) {
            String input = readAllFile(new File(filePath));
            if (input != null) {
                if (PATTERN.matcher(input).matches()) {
                    filesContainsCopyright++;
                    validFiles.add(filePath);
                    System.out.println(filePath + " - Valid file!       [OK]");
                } else {
                    invalidFiles.add(filePath);
                    System.out.println(filePath + " - Missing or corrupted copyright!       [FAIL]");
                }
            }
        }

        System.out.println("Valid Files: ");
        printPathList(validFiles);
        System.out.println("Invalid files: ");
        printPathList(invalidFiles);

        System.out.println("Checked files: "  + filesToControl.size());
        Assert.assertEquals("Files with copyright: " + filesContainsCopyright + ", Files expected to have copyright: " + filesToControl.size(), filesToControl.size(), filesContainsCopyright);
    }

    private void printPathList(List<String> pathList) {
        for (String validFile: pathList) {
            System.out.println(validFile);
        }
    }

    private void loadFiles(File[] files, String extension) {
        for (File file : files) {
            if (file.isDirectory() && !excludedFiles.contains(file.getName())) loadFiles(file.listFiles(), extension);
            if (!file.isDirectory() && file.getName().endsWith(extension) && !excludedFiles.contains(file.getName())) filesToControl.add(file.getAbsolutePath());
        }
    }

    private String readAllFile(File file) {
        byte[] data = new byte[(int) file.length()];
        try {
            if (new FileInputStream(file).read(data) != -1) {
                return new String(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
